package method;

public class Method {
    public static void main(String[] args) {
        sayWelcome();
        int x = 2;
        int y = 3;
        System.out.println("x + y = " + sum(x, y));
        System.out.println("10 + 20 = " + sum(10, 20));

    }
    private static void sayWelcome() {
        System.out.println("Welcome to Calculator Program");
    }
    private static int sum(int a, int b) {
        return a + b;
    }

}
