package method.overloading;

public class OverloadingMethod {
    public static void main(String[] args) {
        Calculator calc = new Calculator();

        // เรียกใช้ method add() ที่มี 2 พารามิเตอร์ int
        System.out.println("Addition of 10 + 20: " + calc.add(10, 20));

        // เรียกใช้ method add() ที่มี 3 พารามิเตอร์ int
        System.out.println("Addition of 10 + 20 + 30: " + calc.add(10, 20, 30));

        // เรียกใช้ method add() ที่มี 2 พารามิเตอร์ double
        System.out.println("Addition of 10.5 + 20.5: " + calc.add(10.5, 20.5));
    }

}



