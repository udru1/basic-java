package method.overloading;

public class Calculator {

    // Overload method add() สำหรับ 2 จำนวน int
    public int add(int a, int b) {
        return a + b;
    }

    // Overload method add() สำหรับ 3 จำนวน int
    public int add(int a, int b, int c) {
        return a + b + c;
    }

    // Overload method add() สำหรับ 2 จำนวน double
    public double add(double a, double b) {
        return a + b;
    }
}
