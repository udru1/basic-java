package method;

public class ParameterArgument {

    public static void main(String[] args) {
        int a = 5;
        int b = 10;

        // การเรียกใช้เมธอด sum โดยส่ง a และ b เป็นอากิวเมนต์
        int result = sum(a, b);
        System.out.println("Result: " + result);
    }

    // ประกาศเมธอด sum โดยมี x และ y เป็นพารามิเตอร์
    public static int sum(int x, int y) {
        return x + y;
    }

}
