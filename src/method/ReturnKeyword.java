package method;

public class ReturnKeyword {

    public static void main(String[] args) {
        int result = multiply(5, 3);
        System.out.println("Result: " + result);
    }

    // เมธอดสำหรับการคูณ 2 จำนวน และ return ผลลัพธ์
    public static int multiply(int x, int y) {
        return x * y;  // คืนค่าผลลัพธ์ของการคูณ
    }
}
