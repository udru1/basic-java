package oop.interfaces.example.sovle;

public class RiderCall {
    public static void main(String[] args) {
        // เรียก rider (grab, food panda, lala move)
//        final String riderBrand = "grab";

        RiderCallServiceCommon serviceCommon = new RiderCallServiceCommon();
        serviceCommon.callRider(new GrabRiderService());

        RiderCallServiceCommon serviceCommon2 = new RiderCallServiceCommon();
        serviceCommon2.callRider(new LalaMoveService());

        RiderCallServiceCommon serviceCommon3 = new RiderCallServiceCommon();
        serviceCommon3.callRider(new FoodPandaService());
    }
}
