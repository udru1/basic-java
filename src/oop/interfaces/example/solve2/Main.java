package oop.interfaces.example.solve2;

public class Main {

    private static String ENV = "local";


    public static void main(String[] args) {
        DBBaseConnect dbService;
        if ("local".equals(ENV)) {
            dbService = new DBBaseConnect() {
                @Override
                public void connectDB() {
                    System.out.println("connect..");
                }
            };
        } else {
            dbService = new DBService();
        }

        System.out.println("connect db...");
        dbService.connectDB();

        System.out.println("calculate sum.");
        //..
    }
}
