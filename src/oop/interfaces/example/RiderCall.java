package oop.interfaces.example;

public class RiderCall {
    public static void main(String[] args) {
        // เรียก rider (grab, food panda, lala move)
        final String riderBrand = "grab";

        if (riderBrand.equals("grab")) {
            System.out.println("call service grab");
            // ... การทำงงาน
        } else if (riderBrand.equals("food_panda")) {

            System.out.println("call service food panda");
            // ... การทำงงาน
        } else if (riderBrand.equals("lalamove")) {

            System.out.println("call service lalamove");
            // ... การทำงงาน
        }

    }
}
