package oop.interfaces;

// คลาสที่นำเสนอ interface Drawable
class Circle implements Drawable {
    public void draw() {
        System.out.println("Drawing a circle");
    }
}
