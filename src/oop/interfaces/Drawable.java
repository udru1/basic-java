package oop.interfaces;

public interface Drawable {
    void draw();
}
