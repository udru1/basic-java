package oop.encapsulation;

class Car {
    // ตัวแปร private ที่ถูก encapsulate
    private String color;
    private int price;
    private int km;
    private String model;

    // Constructor
    Car(String color, int price, int km, String model) {
        this.color = color;
        this.price = price;
        this.km = km;
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getKm() {
        return km;
    }

    public void setKm(int km) {
        this.km = km;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    // สามารถเพิ่ม getter และ setter สำหรับ km และ model ได้เช่นกัน

    // Methods หรือ Behaviors ของคลาส Car
    void start() {
        System.out.println(model + " has started.");
    }

    void drive() {
        System.out.println(model + " is driving.");
    }

    void stop() {
        System.out.println(model + " has stopped.");
    }
}
