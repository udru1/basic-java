package oop.encapsulation;

public class TestCar {

    public static void main(String[] args) {
        Car myCar = new Car("red", 23000, 1200, "Audi");

        // ตั้งค่าใหม่ผ่าน setter
        myCar.setColor("blue");
        myCar.setPrice(25000);

        // อ่านค่าผ่าน getter
        System.out.println("Car color: " + myCar.getColor());
        System.out.println("Car price: " + myCar.getPrice());

        myCar.start();
        myCar.drive();
        myCar.stop();
    }
}
