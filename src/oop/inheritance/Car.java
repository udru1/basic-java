package oop.inheritance;

// Subclass
class Car extends Vehicle {
    int wheels;

    // Constructor
    Car(String brand, int wheels) {
        super(brand); // เรียกใช้ constructor ของ superclass
        this.wheels = wheels;
    }

    void drive() {
        System.out.println(brand + " with " + wheels + " wheels is driving");
    }
}
