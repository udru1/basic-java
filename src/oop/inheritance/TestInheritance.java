package oop.inheritance;

public class TestInheritance {
    public static void main(String[] args) {
        Car myCar = new Car("Toyota", 4);

        myCar.start(); // เรียกใช้เมธอดจาก superclass
        myCar.drive(); // เรียกใช้เมธอดจาก subclass
    }
}
