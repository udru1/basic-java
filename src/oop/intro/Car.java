package oop.intro;

// คลาส Car ที่กำหนดโครงสร้างของวัตถุ Car
class Car {
    // Properties หรือ Fields ของคลาส Car
    String color;
    int price;
    int km;
    String model;

    public Car() {
    }

    // Methods หรือ Behaviors ของคลาส Car
    void start() {
        System.out.println(model + " has started.");
    }

    void drive() {
        System.out.println(model + " is driving.");
    }

    void stop() {
        System.out.println(model + " has stopped.");
    }
}
