package oop.intro;

// คลาสหลักที่ใช้สำหรับการทดสอบคลาส Car
public class TestCar {
    public static void main(String[] args) {
        // สร้าง instance ของคลาส Car
        Car myCar = new Car();

        // กำหนดค่า properties ต่างๆ ให้กับวัตถุ myCar
        myCar.color = "red";
        myCar.price = 23000;
        myCar.km = 1200;
        myCar.model = "Audi";

        // เรียกใช้ methods ของวัตถุ myCar
        myCar.start();
        myCar.drive();
        myCar.stop();
    }
}
