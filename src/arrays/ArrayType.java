package arrays;

public class ArrayType {
    public static void main(String[] args) {
        String[] names = new String[] { "Boss", "Danny", "Jenifer"};

        // Using for loop reading from array
        for (int i = 0; i < names.length; i++) {
            System.out.println("names[" + i + "] = " + names[i]);
        }
    }
}
