package exception;

public class ExceptionExample {
    public static void main(String[] args) {
        try {
            int[] numbers = new int[5];
            numbers[10] = 30;  // ทำให้เกิด ArrayIndexOutOfBoundsException
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Array index is out of bounds!");
        } finally {
            System.out.println("This is the finally block.");
        }

        try {
            int result = divide(10, 0); // ทำให้เกิด ArithmeticException
        } catch (ArithmeticException e) {
            System.out.println("Cannot divide by zero.");
        }
    }

    public static int divide(int a, int b) {
        return a / b;
    }
}

