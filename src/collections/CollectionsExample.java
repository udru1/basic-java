package collections;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.HashMap;

public class CollectionsExample {
    public static void main(String[] args) {
        // การใช้งาน List
        ArrayList<String> list = new ArrayList<>();
        list.add("Apple");
        list.add("Banana");
        list.add("Cherry");
        list.add("Cherry");
        System.out.println("List: " + list);

        // การใช้งาน Set
        HashSet<String> set = new HashSet<>();
        set.add("Apple");
        set.add("Banana");
        set.add("Apple");
        set.add("Banana");
        System.out.println("Set: " + set); // "Apple" จะไม่ซ้ำ

        // การใช้งาน Map
        HashMap<String, Integer> map = new HashMap<>();
        map.put("Apple", 50);
        map.put("Banana", 20);
        map.put("Cherry", 30);
        System.out.println("Map: " + map);
    }
}

